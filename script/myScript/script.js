$(document).ready(function(){
    var btn_num = 0;
    var navbar=$('.nav-bar');
    $('.btn-menu').click(function(){
        
        if(btn_num==0){
            $(this).css({'border': '1px solid rgb(252, 255, 59)'})
            navbar.slideDown();
            btn_num = 1;
        }
        else if(btn_num==1){
            $(this).css({'border': '1px solid rgb(0, 0, 0, 0.09)'}) 
            navbar.slideUp();
            btn_num=0;
        }
        
    });
    function getWindowInnerSize(){
        var w = window.innerWidth;
        var ww = document.getElementById("wis");
        ww.value = w;
        width = $('#wis').val();
        console.log(width);
    }

    setInterval(getWindowInnerSize,500);
    function checkNavMedia(){
        if(width>992){
            navbar.slideDown();
            btn_num = 1;
        }   
    }
    setInterval(checkNavMedia,500);

    var slide=$('.wrapper-slide');
    var title=$('.wrapper-slide-title');
    var slides=['1.png','2.png','3.png','4.png','5.png','6.png'];
    var slides_title=['សូមស្វាគមន៍មកកាន់ពិភពអាហារ','ចង់ញាំុអាហារបែបណាក៏មាន','មានរបៀបធ្វើម្ហូបជាច្រើនសំរាប់ចុងភៅតាមផ្ទះ','ម្ហូបខ្មែរឈ្ងុយឆ្ងាញ់','ម្ហូបខ្មែរ ​ម្ហូបចិន ម្ហូបជប៉ុន ម្ហូបៗៗៗៗ']
    var i=1;
    var r=0;
    function slideAuto(){
        var r = Math.round(Math.random()*4);
        if(i>2)i=0;
        slide.find('img').attr('src','multi-media/image/'+slides[i]);
        slide.find('img').fadeTo(1000,0.5,function(){
            $(this).fadeTo(2500,1)
        });
        
        // title.animate({left:'-250px'}).animate({left:'15px'},3000)
        title.find('h1').text(slides_title[r]);
        r = Math.round(Math.random()*3);
        title.find('h4').text(slides_title[r]);
        title.fadeIn(1000);
        i++;
    }
    setInterval(slideAuto,8000);
  
    $('.wrapper-food-box').on('mouseover',function(){
        $(this).find('.food-detail').css({height:'70px'});
        $(this).find('.food-detail p').show();
    })
    $('.wrapper-food-box').on('mouseout',function(){
        $(this).find('.food-detail').css({height:'35px'});
        $(this).find('.food-detail p').hide();
    })

    $('body').on('click', '.small_image', function () {
        var sr = $(this).find('img').attr('src');
        $('.big_slide').find('img').attr("src", sr);

    });
    // btn scroll to top
    window.onscroll = function() {scrollFunction()};

    function scrollFunction() {
        if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
            document.getElementById("btn-scroll-top").style.display = "block";
        } else {
            document.getElementById("btn-scroll-top").style.display = "none";
        }
    }

    // When the user clicks on the button, scroll to the top of the document
    function topFunction() {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
    }
    document.getElementById('btn-scroll-top').onclick=function(){topFunction()};
});